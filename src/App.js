import React from "react";
import './App.css';
import {Component} from "react";
import Card from "./Card/Card";
import CardDeck from "./CardDeck/CardDeck";
import PokerHand from "./PokerHand/PokerHand";

class App extends Component {

    state = {
        cards: [],
        outCome: "",
    }

    outCome () {
        const pokerHand = new PokerHand(this.state.cards);
        const handResult = pokerHand.getOutCome();
        this.setState({outCome: handResult});
    }

    drawCards = () => {
        const cardDeck = new CardDeck();

        const cards = cardDeck.getCards(5);

        this.setState({cards});
        this.outCome();
    }



  render() {
    return (
        <div className="App">
            <button className="drawCards" onClick={this.drawCards}>draw cards</button>
            <div className="cards">
                {
                    this.state.cards.map(card => {
                        return <Card rank={card.rank} suit={card.suit} />
                    })
                }
            </div>
            <p>{this.state.outCome}</p>
        </div>
    );
  }
}

export default App;
