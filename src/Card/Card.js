import React from "react";
import "./Card.css";

const suits = {
    H: {className: 'Card-hearts', symbol: '♥'},
    C: {className: 'Card-clubs', symbol: '♣'},
    D: {className: 'Card-diams', symbol: '♦'},
    S: {className: 'Card-spades', symbol: '♠'},
}

const Card = props => {

    const suitClass = suits[props.suit].className;
    const symbol = suits[props.suit].symbol;

    const cardClasses = [
        'Card',
        'Card-rank-' + props.rank.toLowerCase(),
        suitClass,
    ];

    if (props.back) {
        cardClasses.push('Card-back');
    }

    return(
        <div className={cardClasses.join(' ')}>
            <span className="Card-rank">{props.rank}</span>
            <span className="Card-suit">{props.suit}</span>
        </div>
    );
}

export default Card;