class CardDeck {

    static SUITS = {
        DIAMONDS: 'D',
        HEARTS: 'H',
        CLUBS: 'C',
        SPADES: 'S'
    };

    static RANKS = {
        TWO: '2',
        THREE: '3',
        FOUR: '4',
        FIVE: '5',
        SIX: '6',
        SEVEN: '7',
        EIGHT: '8',
        NINE: '9',
        TEN: '10',
        JACK: 'J',
        QUEEN: 'Q',
        KING: 'K',
        ACE : 'A',
    };

    cards = [];

    constructor() {
        for (let suit in CardDeck.SUITS) {
            for (let rank in CardDeck.RANKS) {
                this.cards.push({
                    suit: CardDeck.SUITS[suit],
                    rank: CardDeck.RANKS[rank]
                });
            }

        }
    }

    getCard() {
        let randomCardIndex = Math.floor(Math.random() * this.cards.length);
        let [card] = this.cards.splice(randomCardIndex, 1);

        return card;
    }

    getCards(howMany) {
        const cards = [];

        for (let i = 0; i < howMany; i++) {
            cards.push(this.getCard());
        }

        return cards;
    }

}

export default CardDeck;