class PokerHand {

    static RANKS = {
        TWO: '2',
        THREE: '3',
        FOUR: '4',
        FIVE: '5',
        SIX: '6',
        SEVEN: '7',
        EIGHT: '8',
        NINE: '9',
        TEN: '10',
        JACK: 'J',
        QUEEN: 'Q',
        KING: 'K',
        ACE : 'A',
    };

    static RANKS_FOUND = {
        TWO: 0,
        THREE: 0,
        FOUR: 0,
        FIVE: 0,
        SIX: 0,
        SEVEN: 0,
        EIGHT: 0,
        NINE: 0,
        TEN: 0,
        JACK: 0,
        QUEEN: 0,
        KING: 0,
        ACE :0,
    };

    cards = [];

    constructor(handCard) {
        this.cards = handCard;
    }

    getOutCome() {
        let twoPair = 0;
        // let onePair = 0;

        this.cards.forEach((card, i) => {

            for (const key in card) {
                for (const handKey in PokerHand.RANKS) {
                    if (card[key] === PokerHand.RANKS[handKey]) {
                        PokerHand.RANKS_FOUND[handKey] ++;
                    }
                }

            }
        });


        for(const foundRank in PokerHand.RANKS_FOUND)
        {
            if (PokerHand.RANKS_FOUND[foundRank] === 3) {
                return "Тройка (three of a kind)";
            }

            if (PokerHand.RANKS_FOUND[foundRank] === 2) {
                twoPair++;
            }

        }
        if (twoPair === 2) {
            return "Две пары (two pairs)";
        }

        if (twoPair === 1) {
            return "Одна пара (англ. one pair)";
        }

        // console.log(twoPair);
        console.log(PokerHand.RANKS_FOUND)
        // console.log(`${foundRank} , ${PokerHand.RANKS_FOUND[foundRank]}`)
    }


}

export default PokerHand;